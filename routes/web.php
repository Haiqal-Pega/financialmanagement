<?php

use App\Http\Controllers\AccountController;
use App\Http\Controllers\BillsController;
use App\Http\Controllers\BudgetsController;
use App\Http\Controllers\ExpensesController;
use App\Http\Controllers\ForecastsController;
use App\Http\Controllers\PlanningsController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\SavingController;
use App\Http\Controllers\UserController;
use App\Models\Account;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reset', function () {
    return view('passwordReset');
})->name('password-reset-view');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('resetPassword', [UserController::class, 'resetPassword'])->name('reset-password');


Route::middleware(['web', 'auth'])->group(function () {
    Route::get('/admin', [UserController::class, 'admin'])->name('admin');
    Route::delete('/admin/delete/{id}', [UserController::class, 'delete'])->name('delete.user');

    //budget module route
    Route::get('/Budget', [BudgetsController::class, 'index'])->name('budget-index');
    Route::get('/budgets/create', [BudgetsController::class, 'budgets_create'])->name('budgets.create');
    Route::get('/budgets/edit', [BudgetsController::class, 'edit'])->name('budgets.edit');
    Route::post('/budgets/store', [BudgetsController::class, 'budgets_store'])->name('budget-store');

    //expenses module route
    Route::get('/Expenses', [ExpensesController::class, 'index'])->name('expenses.index');
    Route::post('/budgets/expenses', [ExpensesController::class, 'store'])->name('expenses.store');

    //account
    Route::get('/accounts/{account}/edit', [AccountController::class, 'edit'])->name('accounts.edit');
    Route::put('/accounts/{account}', [AccountController::class, 'update'])->name('accounts.update');

    //saving
    Route::get('/savings', [SavingController::class, 'create'])->name('savings.create');
    Route::get('/savings/add', [SavingController::class, 'add'])->name('savings.add');
    Route::post('/savings/store', [SavingController::class, 'store'])->name('savings.store');
    Route::post('/savings/update', [SavingController::class, 'update'])->name('savings.update');

    //report
    Route::get('/Report', [ReportController::class, 'index'])->name('report-index');
    Route::get('/Report/Filtered', [ReportController::class, 'filter'])->name('report-filterByMonth');

    //Bills
    Route::get('/Bills', [BillsController::class, 'index'])->name('bills-index');
    Route::get('/Bills/Filtered', [BillsController::class, 'filter'])->name('bills.filterByMonth');
    Route::get('/Bills/create', [BillsController::class, 'create'])->name('bills.create');
    Route::post('/Bills/store', [BillsController::class, 'store'])->name('bills.store');
    Route::get('/Bills/payment/{id}', [BillsController::class, 'payment'])->name('bills.payment');

    //Planning
    Route::get('/Planning', [PlanningsController::class, 'index'])->name('planning-index');
    Route::get('/Planning/Create', [PlanningsController::class, 'create'])->name('planning-create');
    Route::post('/Planning/Store', [PlanningsController::class, 'store'])->name('planning-store');

    //Forecasts
    Route::get('/Forecasts', [ForecastsController::class, 'index'])->name('forecasts-index');


});