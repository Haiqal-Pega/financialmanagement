<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use App\Models\Expenses;
use App\Models\Plannings;
use App\Models\Saving;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;



class BudgetsController extends Controller
{
    public function index()
    {
        $user = Auth::user();

        $budgets = Budget::where('user_id', $user->id)
            ->where('month', date('F'))
            ->orderBy('created_at', 'desc')
            ->get();

        $budgetIds = Budget::where('user_id', $user->id)->pluck('id'); // use for retrieving transaction based on user

        $expenses = Expenses::whereIn('category', $budgetIds)
            ->orderBy('created_at', 'desc')
            ->get();

        $savings = Saving::where('user_id', $user->id)->orderBy('current_amount', 'desc')->get();

        $currentMonth = Carbon::now()->format('m'); // Current month as 'm' (e.g., '09' for September)
        $currentYear = Carbon::now()->format('Y'); // Current year as 'Y' (e.g., '2023')

        // Assuming you want the latest 'Needs' budget record for the current month and year
        $budgetNeeds = Budget::where('user_id', $user->id)
            ->where('name', 'Needs')
            ->whereYear('created_at', $currentYear)
            ->whereMonth('created_at', $currentMonth)
            ->orderBy('created_at', 'desc')
            ->first();

        // dd($budgetNeeds);

        if ($budgetNeeds) {
            $totalExpensesNeeds = Expenses::where('category', $budgetNeeds->id)
                ->whereYear('created_at', $currentYear)
                ->whereMonth('created_at', $currentMonth)
                ->sum('amount');
        } else {
            $totalExpensesNeeds = 0; // No 'Needs' budget found for the current month and year
        }

        // Assuming you want the latest 'Wants' budget record for the current month and year
        $budgetWants = Budget::where('user_id', $user->id)
            ->where('name', 'Wants')
            ->whereYear('created_at', $currentYear)
            ->whereMonth('created_at', $currentMonth)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($budgetWants) {
            $totalExpensesWants = Expenses::where('category', $budgetWants->id)
                ->whereYear('created_at', $currentYear)
                ->whereMonth('created_at', $currentMonth)
                ->sum('amount');
        } else {
            $totalExpensesWants = 0; // No 'Wants' budget found for the current month and year
        }

        // Assuming you want the latest 'Savings' budget record for the current month and year
        $budgetSavings = Budget::where('user_id', $user->id)
            ->where('name', 'Savings')
            ->whereYear('created_at', $currentYear)
            ->whereMonth('created_at', $currentMonth)
            ->orderBy('created_at', 'desc')
            ->first();

        if ($budgetSavings) {
            $totalExpensesSavings = Expenses::where('category', $budgetSavings->id)
                ->whereYear('created_at', $currentYear)
                ->whereMonth('created_at', $currentMonth)
                ->sum('amount');
        } else {
            $totalExpensesSavings = 0; // No 'Savings' budget found for the current month and year
        }

        $plannings = Plannings::where('user_id', $user->id)->first();

        return view('budget.index', compact('budgets', 'expenses', 'savings', 'user', 'plannings'));
    }




    public function budgets_create()
    {
        return view('budget.add-budget');
    }

    public function budgets_store(Request $request)
    {
        $request->validate([
            'salary' => 'required',
            'needsHidden' => 'required|numeric',
            'wantsHidden' => 'required|numeric',
            'savingsHidden' => 'required|numeric',
        ]);

        // Get the currently authenticated user
        $user = auth()->user();

        // Check if current month's budget entries exist
        $budgetCurrentMonthExist = Budget::where('user_id', $user->id)
            ->where('month', date('F'))
            ->get();

        // Update or create values for each budget
        if ($budgetCurrentMonthExist->isEmpty()) {
            // Create budget entries if they don't exist
            foreach (['Needs', 'Wants', 'Savings'] as $budgetName) {
                Budget::create([
                    'user_id' => $user->id,
                    'name' => $budgetName,
                    'month' => date('F'),
                    'amount' => $request->input(strtolower($budgetName) . 'Hidden'),
                ]);
            }
        } else {
            // Update budget entries if they exist
            DB::table('budgets')
                ->where('user_id', $user->id)
                ->where('month', date('F'))
                ->whereIn('name', ['Needs', 'Wants', 'Savings'])
                ->update([
                    'amount' => DB::raw('CASE 
                               WHEN name = "Needs" THEN ' . $request->input('needsHidden') . '
                               WHEN name = "Wants" THEN ' . $request->input('wantsHidden') . '
                               WHEN name = "Savings" THEN ' . $request->input('savingsHidden') . '
                               ELSE amount 
                             END'),
                ]);
        }

        // Increment bank balance
        DB::table('accounts')
            ->where('user_id', $user->id)
            ->where('name', 'Bank')
            ->increment('balance', $request->input('salary'));

        return redirect()->route('budget-index')->with('success', 'Budgets updated or created successfully');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
     */
    // public function storeExpense(Request $request, $id)
    // {
    //     $request->validate([
    //         'name' => 'required',
    //         'description' => 'nullable',
    //         'amount' => 'required|numeric',
    //         'category' => 'required',
    //     ]);

    //     $budget = Budget::findOrFail($id); // Find the budget by its ID

    //     // Create a new expense associated with the budget
    //     $expense = $budget->expenses()->create([
    //         'name' => $request->input('name'),
    //         'description' => $request->input('description'),
    //         'amount' => $request->input('amount'),
    //         'category' => $request->input('category'),
    //     ]);

    //     // Optionally, you can perform additional actions or redirect to a different page

    //     return redirect()->route('budget-index')->with('success', 'Expense added successfully');
    // }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Budgets  $budgets
     * @return \Illuminate\Http\Response
     */
    public function show(Budgets $budgets)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     */
    public function edit()
    {
        $user = auth()->user();

        // Check if the salary for the current month has already been set
        $currentMonth = date('F');
        $salarySet = DB::table('budgets')
            ->where('user_id', $user->id)
            ->where('month', $currentMonth)
            ->where('amount', '>', 0)
            ->exists();

        return view('budget.edit-budget', ['salarySet' => $salarySet]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Budgets  $budgets
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Budgets $budgets)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Budgets  $budgets
     * @return \Illuminate\Http\Response
     */
    public function destroy(Budgets $budgets)
    {
        //
    }
}