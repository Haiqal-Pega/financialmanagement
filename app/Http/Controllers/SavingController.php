<?php

namespace App\Http\Controllers;

use App\Models\Saving;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SavingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
    //  * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('savings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $user = Auth::user();

        // Validate the form data
        $validatedData = $request->validate([
            'name' => 'required|string',
            'target_amount' => 'required|numeric|min:0',
            'deadline' => 'required|date',
        ]);
        // Create the saving plan
        $saving = new Saving();
        $saving->name = $validatedData['name'];
        $saving->target_amount = $validatedData['target_amount'];
        $saving->current_amount = 0; // Set the initial current amount to 0
        $saving->deadline = $validatedData['deadline'];
        $saving->user_id = $user->id;
        $saving->save();

        return redirect()->route('budget-index')->with('success', 'Saving plan created successfully.');
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Saving  $saving
     * @return \Illuminate\Http\Response
     */
    public function show(Saving $saving)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Saving  $saving
     * @return \Illuminate\Http\Response
     */
    public function edit(Saving $saving)
    {
        //
    }

    public function add()
    {
        return view('savings.add');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Saving  $saving
    //  * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $userId = Auth::id(); // Get the authenticated user's ID
        $amount = $request->input('amount');

        $saving = Saving::where('user_id', $userId)->first();


        $saving->current_amount += $amount;
        $success = $saving->save();

        if ($success) {
            $query = "UPDATE accounts SET balance = balance - :amount 
                  WHERE user_id = :userId AND name = :bankAccountName";
            DB::statement($query, [
                'amount' => $amount,
                'userId' => $userId,
                'bankAccountName' => 'Bank',
            ]);

        }

        // Redirect or return a response as needed
        return redirect()->route('budget-index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Saving  $saving
    //  * @return \Illuminate\Http\Response
     */
    public function destroy(Saving $saving)
    {
        //
    }
}