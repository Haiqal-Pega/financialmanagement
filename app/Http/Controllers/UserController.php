<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Password;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        return view('dashboard');

    }



    public function login(Request $request)
    {
        // Retrieve input data from the form
        $email = $request->input('email');
        $password = $request->input('password');

        // Query the database to check if a user with the provided email exists
        $user = User::where('email', $email)->first();

        if ($user) {
            // User exists in the database
            if (Hash::check($password, $user->password)) {
                // Password is correct

                if ($user->role == 1) {
                    // Admin user
                    // Perform the desired action for admin, such as redirecting to the admin panel
                    return redirect()->route('dashboard');
                } elseif ($user->role == 0) {
                    // Regular user/member
                    // Perform the desired action for regular user, such as logging in the user or redirecting to a success page
                    return redirect()->route('admin');

                }
            } else {
                // Password is incorrect
                // Perform the appropriate action, such as displaying an error message or redirecting back to the login form
                return redirect()->route('login')->withErrors(['password' => 'Invalid password']);
            }
        } else {
            // User does not exist in the database
            // Perform the appropriate action, such as displaying an error message or redirecting back to the login form
            return redirect()->route('login')->withErrors(['email' => 'User not found']);
        }
    }


    public function admin()
    {
        $users = User::all()->where('role', '<>', 5);
        return view('admin.admin')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            // Validate the user input
            $validatedData = $request->validate([
                'name' => 'required|string|max:255',
                'email' => 'required|string|email|unique:users|max:255',
                'password' => 'required|string|min:2',
            ]);
            // Create a new user
            $user = User::create([
                'name' => $validatedData['name'],
                'email' => $validatedData['email'],
                'password' => bcrypt($validatedData['password']),
                'role' => 1 // Set the role to 1 by default
            ]);



            // Perform any additional actions (e.g., sending email verification, etc.)

            // Redirect the user to the desired page
            //return redirect()->route('login')->with('success', 'Registration successful. You can now login.');
            return view('users.login');
        } catch (QueryException $e) {
            // Handle the duplicate entry exception
            if ($e->getCode() === '23000') {
                return back()->withErrors(['email' => 'Email already exists.'])->withInput();
            }

            // Handle other query exceptions if needed

            // Log the exception or display a generic error message
            logger()->error('Query Exception: ' . $e->getMessage());

            return back()->withErrors('An error occurred during registration. Please try again.')->withInput();
        }

    }

    public function logout()
    {
        // Clear the user's session and any other necessary actions
        Session::flush();

        // Perform any additional actions, such as redirecting to a specific page
        return redirect()->route('login.submit');
    }

    public function resetPassword(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:8|confirmed',
        ]);

        $user = User::where('email', $request->email)->first();

        if ($user) {
            $user->password = Hash::make($request->password);
            $user->save();

            return redirect()->route('login')->with('success', 'Your password has been reset successfully. You can now login with your new password.');
        }

        return redirect()->back()->withErrors(['email' => 'User not found']);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        // Find the user by ID
        $user = User::find($id);

        if ($user) {
            // Delete the user
            $user->delete();

            // Redirect back to the user management page with a success message
            return redirect()->route('admin')->with('success', 'User deleted successfully');
        }

        // If the user is not found, redirect back to the user management page with an error message
        return redirect()->route('admin')->with('error', 'User not found');
    }

}