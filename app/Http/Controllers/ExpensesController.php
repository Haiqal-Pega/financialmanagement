<?php

namespace App\Http\Controllers;

use App\Models\Saving;
use Illuminate\Http\Request;
use App\Models\Budget;
use App\Models\Expenses;
use App\Models\Account;
use App\Models\Plannings;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class ExpensesController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $budgets = $user->budget()->where('month', date('F'))->pluck('name', 'id');
        $accounts = $user->accounts()->pluck('name', 'id');
        $savings = Saving::where('user_id', $user->id)->orderBy('id', 'asc')->get();
        $plannings = Plannings::where('user_id', $user->id)->first();

        return view('expenses.index', compact('budgets', 'accounts', 'user', 'savings', 'plannings'));
    }


    // public function store1(Request $request)
    // {
    //     $request->validate([
    //         'name' => 'nullable',
    //         'description' => 'nullable',
    //         'savingsPlan' => 'nullable',
    //         'category' => 'required',
    //         'amount' => ['required', 'numeric', 'regex:/^\d+(\.\d{1,2})?$/'],
    //         'account' => 'required',
    //     ]);

    //     $currentMonth = now()->format('m'); // Current month as 'm' (e.g., '09' for September)
    //     $currentYear = now()->format('Y'); // Current year as 'Y' (e.g., '2023')

    //     // Get the currently authenticated user
    //     $user = auth()->user();

    //     // Find the budget record for the given category, user, and current month/year
    //     $budget = Budget::where('name', $request->input('category'))
    //         ->whereYear('created_at', $currentYear)
    //         ->whereMonth('created_at', $currentMonth)
    //         ->where('user_id', $user->id)
    //         ->first();

    //     // Create the expense
    //     $expense = new Expenses();
    //     $expense->category = $budget->id;
    //     $expense->amount = $request->input('amount');
    //     $expense->account_id = $request->input('account');

    //     $savingName = DB::table('Savings')->where('id', $request->input('savingsPlan'))->where('user_id', $user->id)->first();

    //     if ($request->input('category') == 'Savings') {
    //         $expense->name = $savingName->name;
    //         $expense->description = 'Savings';
    //     } else {
    //         $expense->name = $request->input('name');
    //         $expense->description = $request->input('description');
    //     }

    //     $expense->save();

    //     // Fetch the budget for the given category
    //     $budget = Budget::find($budget->id);
    //     if ($budget->name == 'Savings') {
    //         // Update the savings amount
    //         $user = auth()->user();
    //         $savings = Saving::where('user_id', $user->id)->first();
    //         DB::table('savings')
    //             ->where('user_id', $user->id)
    //             ->increment('current_amount', $request->input('amount'));

    //     }

    //     return redirect()->route('budget-index')->with('success', 'Budget added successfully');
    // }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'nullable',
            'description' => 'nullable',
            'savingsPlan' => 'nullable',
            'category' => 'required',
            'amount' => ['required', 'numeric', 'regex:/^\d+(\.\d{1,2})?$/'],
            'account' => 'required',
        ]);

        $user = auth()->user();
        $currentMonth = now()->format('m'); // Current month as 'm' (e.g., '09' for September)
        $currentYear = now()->format('Y'); // Current year as 'Y' (e.g., '2023')
        $budget = Budget::where('name', $request->input('category'))
            ->whereYear('created_at', $currentYear)
            ->whereMonth('created_at', $currentMonth)
            ->where('user_id', $user->id)
            ->first();

        // Get the currently authenticated user
        $user = auth()->user();

        if ($request->input('savingsPlan') == 'retirement') {
            // Handle retirement savings
            // Add the amount to the currentBalance in Plannings table
            $planning = Plannings::where('user_id', $user->id)->first();
            if ($planning) {
                DB::table('plannings')
                    ->where('user_id', $user->id)
                    ->increment('currentBalance', $request->input('amount'));
            }

            // Add an entry into the expenses table as a saving
            $expense = new Expenses();
            $savingName = DB::table('Savings')->where('id', $request->input('savingsPlan'))->where('user_id', $user->id)->first();

            $expense->category = $budget->id; // Assuming 'Savings' is the category for savings expenses
            $expense->name = 'Retirement Planning'; // You can customize the name
            $expense->description = 'Retirement Saving'; // You can customize the description
            $expense->amount = $request->input('amount');
            $expense->account_id = $request->input('account');
            $expense->save();
        } else {
            // Handle regular expenses
            // Find the budget record for the given category, user, and current month/year

            // Create the expense
            $expense = new Expenses();
            $expense->category = $budget->id;
            $expense->amount = $request->input('amount');
            $expense->account_id = $request->input('account');

            $savingName = DB::table('Savings')->where('id', $request->input('savingsPlan'))->where('user_id', $user->id)->first();
            // dd($savingName);
            
            if ($request->input('category') == 'Savings') {
                $expense->name = $savingName->name;
                $expense->description = 'Savings';

                $saving = Saving::where('id', $savingName->id)->first();
                
                if ($saving) {
                    $saving->increment('current_Amount', $request->input('amount'));
                }


            } else {
                $expense->name = $request->input('name');
                $expense->description = $request->input('description');
            }

            $expense->save();
        }

        return redirect()->route('budget-index')->with('success', 'Budget added successfully');
    }




}