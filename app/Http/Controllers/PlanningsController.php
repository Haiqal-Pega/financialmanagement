<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use App\Models\Plannings;
use App\Models\Saving;
use Illuminate\Http\Request;

class PlanningsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
    //  * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $user = auth()->user();
        $retirementPlan = Plannings::where('user_id', $user->id)->first();
        $savings = Saving::where('user_id', $user->id)->get();

        $budgets = Budget::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        return view('planning.index', compact('retirementPlan', 'savings', 'budgets'));
    }

    /**
     * Show the form for creating a new resource.
     *
    //  * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('planning.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'currentAge' => 'required|numeric',
            'targetAge' => 'required|numeric',
            'projected_savings' => 'required|numeric'
        ]);

        $yearsToSave =  $request->input('targetAge') -  $request->input('currentAge');
        $monthly = $request->input('projected_savings')/($yearsToSave*12);
        $monthly = str_replace(',', '', number_format($monthly, 2));

        // Create and store the retirement plan
        $retirementPlan = new Plannings();
        $retirementPlan->user_id = auth()->user()->id; 
        $retirementPlan->currentAge = $request->input('currentAge');
        $retirementPlan->targetAge = $request->input('targetAge');
        $retirementPlan->projected_savings = $request->input('projected_savings');
        $retirementPlan->monthly_savings = $monthly;
        $retirementPlan->currentBalance = 0;
        $retirementPlan->save();

        return redirect()->route('planning-index')->with('success', 'Retirement plan created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Plannings  $plannings
     * @return \Illuminate\Http\Response
     */
    public function show(Plannings $plannings)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Plannings  $plannings
     * @return \Illuminate\Http\Response
     */
    public function edit(Plannings $plannings)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Plannings  $plannings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Plannings $plannings)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Plannings  $plannings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Plannings $plannings)
    {
        //
    }
}