<?php

namespace App\Http\Controllers;

use App\Models\Report;
use App\Models\Saving;
use App\Models\Budget;
use App\Models\Expenses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
    //  * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // For budget progress
        $budgetMonth = Budget::where('user_id', $user->id)
            ->where('month', date('F'))
            ->orderBy('created_at', 'desc')
            ->get();

        //for transaction list
        $budgets = Budget::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $budgetIds = $budgets->pluck('id');

        $months = Budget::select('month')
            ->where('user_id', $user->id)
            ->distinct('month')
            ->orderBy('month', 'desc')
            ->get();

        $expenses = Expenses::whereIn('category', $budgetIds)
            ->orderBy('created_at', 'desc')
            ->get();

        $savings = Saving::where('user_id', $user->id)->sum('current_amount');

        $filtered = null;

        return view('report.index', compact('budgets', 'budgetMonth', 'expenses', 'savings', 'user', 'months', 'filtered'));
    }

    public function filter(Request $request)
    {
        $user = Auth::user();

        // For budget progress
        $budgetMonth = Budget::where('user_id', $user->id)
            ->where('month', $request->input('selected_month'))
            ->orderBy('created_at', 'desc')
            ->get();

        //for transaction list
        $budgets = Budget::where('user_id', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $budgetIds = $budgets->pluck('id');

        $months = Budget::select('month')
            ->where('user_id', $user->id)
            ->distinct('month')
            ->orderBy('month', 'desc')
            ->get();

        $expenses = Expenses::whereIn('category', $budgetIds)
            ->orderBy('created_at', 'desc')
            ->get();

        $savings = Saving::where('user_id', $user->id)->sum('current_amount');

        $filtered = $budgetMonth->first()->month;

        return view('report.index', compact('budgets', 'budgetMonth', 'expenses', 'savings', 'user', 'months', 'filtered'));
    }


    /**
     * Show the form for creating a new resource.
     *
    //  * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Report  $report
    //  * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Report  $report
    //  * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Report  $report
    //  * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Report $report)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Report  $report
    //  * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        //
    }
}