<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Budget;
use App\Models\Expenses;
use App\Models\Saving;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (Auth::check()) {
            $user = Auth::user();
            $role = $user->role;

            if ($role == 5) {
                // Redirect to the admin page
                return redirect()->route('admin');
            }

            $budgets = Budget::where('user_id', $user->id)
                ->orderBy('created_at', 'desc')
                ->get();

            $budgetIds = $budgets->pluck('id');

            $expenses = Expenses::whereIn('category', $budgetIds)
                ->orderBy('created_at', 'desc')
                ->get();

            $savings = Saving::where('user_id', $user->id)->first();
            $accounts = Account::where('user_id', $user->id)->get();

            return view('home', compact('user', 'budgets', 'savings', 'accounts','expenses'));
        }

        // Redirect to the home page for other roles
        return view('home');
    }

}