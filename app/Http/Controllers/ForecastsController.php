<?php

namespace App\Http\Controllers;

use App\Models\Budget;
use App\Models\Forecasts;
use App\Models\Saving;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ForecastsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     */
    public function index()
    {
        $user = Auth::user();
        
        $budgetNeeds = Budget::where('user_id', $user->id)
        ->where('name', 'Needs')
        ->sum('amount');
        $budgetWants = Budget::where('user_id', $user->id)
        ->where('name', 'Wants')
        ->sum('amount');
        $budgetSavings = Budget::where('user_id', $user->id)
        ->where('name', 'Savings')
        ->sum('amount');
        
        $budgetTotalMonth = Budget::where('user_id', $user->id)
        ->distinct()
        ->count('month');

        $savingPlans = Saving::where('user_id', $user->id)->get();
        // dd($savingPlan);

        return view('forecasts.index', compact('budgetNeeds', 'budgetWants', 'budgetSavings', 'budgetTotalMonth', 'savingPlans' ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Forecasts  $forecasts
     * @return \Illuminate\Http\Response
     */
    public function show(Forecasts $forecasts)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Forecasts  $forecasts
     * @return \Illuminate\Http\Response
     */
    public function edit(Forecasts $forecasts)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Forecasts  $forecasts
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Forecasts $forecasts)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Forecasts  $forecasts
     * @return \Illuminate\Http\Response
     */
    public function destroy(Forecasts $forecasts)
    {
        //
    }
}
