<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use App\Models\Account;
use App\Models\Saving;



class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'role',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        self::created(function ($user) {
            $user->createDefaultAccounts();
        });
    }

    public function createDefaultAccounts()
    {
        $this->accounts()->createMany([
            ['name' => 'Cash'],
            ['name' => 'Bank'],
        ]);

        $currentMonth = date('F'); // Get current month in full text format
        $this->budget()->createMany([
            ['name' => 'Needs', 'amount' => 0, 'month' => $currentMonth],
            ['name' => 'Wants', 'amount' => 0, 'month' => $currentMonth],
            ['name' => 'Savings', 'amount' => 0, 'month' => $currentMonth],
        ]);

    }

    public function budget()
    {
        return $this->hasMany(Budget::class, 'user_id');
    }

    public function accounts()
    {
        return $this->hasMany(Account::class);
    }

    public function savingPlan()
    {
        return $this->hasOne(Saving::class);
    }


}