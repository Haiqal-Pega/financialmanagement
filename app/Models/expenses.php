<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
    use HasFactory;

    protected $table = 'expenses';
    protected $fillable = [
        'name',
        'description',
        'amount',
        'category',
    ];

    // Define the relationship with the Budget model
    public function budget()
{
    return $this->belongsTo(Budget::class, 'category');
}


    public function account()
    {
        return $this->belongsTo(Account::class);
    }

}