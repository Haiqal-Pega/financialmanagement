<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Plannings extends Model
{
    use HasFactory;
    protected $table = 'plannings';
    protected $fillable = ['currentAge', 'targetAge', 'currentBalance', 'monthly_savings', 'projected_savings'];
}
