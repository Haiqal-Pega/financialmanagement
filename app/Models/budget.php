<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;


class Budget extends Model
{
    use HasFactory;
    protected $table = 'budgets';
    protected $fillable = ['name', 'amount', 'month', 'user_id'];

    public function expenses()
    {
        return $this->hasMany(Expenses::class, 'category', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function balancePercentage()
    {
        $totalExpenses = $this->expenses()->sum('amount');
        if($this->amount == 0) return 0;
        else{
            $balancePercentage = ($totalExpenses / $this->amount) * 100;
            return round($balancePercentage, 2);
        }
    }

    public function balance()
    {
        $totalExpenses = $this->expenses()->sum('amount');
        $balance = $this->amount - $totalExpenses;
        return $balance;
    }

}