<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Expenses;

class Account extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'balance'];

    public function expenses()
    {
        return $this->hasMany(Expenses::class);
    }

    public function getCurrentBalance()
    {
        $totalExpenses = $this->expenses()->sum('amount');
        return $this->balance - $totalExpenses;
    }
}