@extends('layouts.app')

@section('title', 'Reset Paswword')

@section('content')
    <div class="container" style="max-width: 40%">
        <h2 class="text-center py-4">Reset Password</h2>
        <form method="POST" action="{{route('reset-password')}}">
            @csrf

            <div class="card p-4 ">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" class="form-control" required value="{{ old('email') }}">
                    @error('email')
                        <span class="text-danger">{{ $message }}</span>
                    @enderror
                </div>

                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" class="form-control" required>
                </div>

                <div class="form-group">
                    <label for="password_confirmation">Confirm Password</label>
                    <input type="password" id="password_confirmation" name="password_confirmation" class="form-control"
                        required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Reset Password</button>
                    <p class="float-right py-2"><a href="{{ route('login') }}">Remember your password?</a></p>
                </div>
            </div>

        </form>
    </div>
@endsection
