@extends('layouts.nav')

@section('title', 'Financial Manager')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="containter">
        <h1>Your Forecasts</h1>
        <small>This forecasts is based on historical data</small>

        <div class="row">
            <div class="col-md-12">
                <div class="card mt-4">
                    <div class="card-header" style="background-color: #F8CEDC">
                        <b>Forecasted Future Expenses</b>
                    </div>
                    <div class="card-body">
                        <div class="row d-flex justify-content-center">
                            <div class="col-md-5 m-2">
                                <div class="card my-3">
                                    <div class="card-header" style="background-color:beige">
                                        <b>For the Next Month, Expenses for Your Needs Should Be</b>
                                    </div>
                                    <div class="card-body d-flex justify-content-center">
                                        RM {{ number_format($budgetNeeds / $budgetTotalMonth, 2) }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 m-2">
                                <div class="card my-3 ">
                                    <div class="card-header"  style="background-color:beige">
                                        <b>For the Next Month, Expenses for Your Wants Should Be</b>
                                    </div>
                                    <div class="card-body d-flex justify-content-center">
                                        RM {{ number_format($budgetWants / $budgetTotalMonth, 2) }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    
            <div class="col-md-4">
                <div class="card mt-4">
                    <div class="card-header" style="background-color: #F8CEDC">
                        <b>Forecasted Future Income</b>
                    </div>
                    <div class="card-body my-2 mb-4">
                        Based on previous salaries, your next salary should be around:<b>
                        RM {{ number_format(($budgetNeeds + $budgetWants + $budgetSavings) / $budgetTotalMonth, 2) }} </b><br><br>
                        Your Annual Salary Next Year with 10% Increment is Around: <b>RM 
                        {{ number_format((($budgetNeeds + $budgetWants + $budgetSavings) / $budgetTotalMonth) * 12 * 1.1, 2) }}</b>
                    </div>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="card mt-4">
                    <div class="card-header" style="background-color: #F8CEDC">
                        <b>Forecasted Future Savings</b>
                    </div>
                    <div class="card-body">
                        @if ($savingPlans->count() > 0)
                            @foreach ($savingPlans as $savingPlan)
                                @php
                                    // Calculate the number of months remaining until the due date
                                    $dueDate = \Carbon\Carbon::parse($savingPlan->deadline);
                                    $today = \Carbon\Carbon::now();
                                    $monthsRemaining = $dueDate->diffInMonths($today);
                                    
                                    // Calculate the monthly savings needed or display a message
                                    if ($monthsRemaining > 0) {
                                        $monthlySavingsNeeded = $savingPlan->target_amount / $monthsRemaining;
                                    } else {
                                        // Check if the due date is in the same month as the current date
                                        if ($dueDate->isSameMonth($today)) {
                                            $monthlySavingsNeeded = 'This Saving Plan is Less Than a Month From The Targeted Date';
                                        } else {
                                            $monthlySavingsNeeded = 'Target Date Expired';
                                        }
                                    }
                                @endphp
        
                                <div class="row d-flex justify-content-center">
                                    <div class="col-md-5 m-2">
                                        <div class="card my-3">
                                            <div class="card-header" style="background-color: beige">
                                                <b>Amount Needed Monthly for {{ $savingPlan->name }} Before Targeted Date</b>
                                            </div>
                                            <div class="card-body d-flex justify-content-center">
                                                {{ is_numeric($monthlySavingsNeeded) ? 'RM ' . number_format($monthlySavingsNeeded, 2) : $monthlySavingsNeeded }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <p>No saving plans set.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
