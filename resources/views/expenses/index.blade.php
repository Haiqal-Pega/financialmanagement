@extends('layouts.nav')

@section('title', 'Financial Manager')

@section('content')
    <div class="container">
        <h1>Add Expense</h1>
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('expenses.store') }}">
                    @csrf
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select id="category" name="category" class="form-control" required>
                            @forelse ($budgets as $id => $name)
                                <option value="{{ $name }}" id="selectedCategory">{{ $name }}</option>
                            @empty
                                <option value="null">Create some budget before any expenses</option>
                            @endforelse
                        </select>
                    </div>

                    <div class="form-group" id="savingsPlanSelect" style="display: none;">
                        <label for="savingsPlan">Savings Plans</label>
                        <select id="savingsPlan" name="savingsPlan" class="form-control">
                            <option id="notSaving" value="null" selected disabled>Choose Your Saving Plan</option>
                            @if ($plannings)
                                <option value="retirement">Retirement Plan</option>
                            @endif
                            @foreach ($savings as $plan)
                                <option value="{{ $plan->id }}">{{ $plan->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group" id="expensesForm">
                        <label for="name">Expense Name</label>
                        <input type="text" id="name" name="name" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" id="amount" name="amount" class="form-control" required step="0.01">
                    </div>

                    <div class="form-group" id="descForm">
                        <label for="description">Description</label>
                        <textarea id="description" name="description" class="form-control" rows="4"></textarea>
                    </div>

                    <div class="form-group">
                        <label for="account">Account</label>
                        <select id="account" name="account" class="form-control" required>
                            @foreach ($user->accounts as $account)
                                <option value="{{ $account->id }}">{{ $account->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add Expense</button>
                        <a href="{{ route('budget-index') }}" class="btn btn-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        document.getElementById('category').addEventListener('change', function() {
            var value = document.getElementById('selectedCategory').innerHTML; // get value selected
            if (this.value == 'Savings') {
                savingsPlanSelect.style.display = 'block';
                expensesForm.style.display = 'none';
                descForm.style.display = 'none';
            } else {
                savingsPlanSelect.style.display = 'none';
                descForm.style.display = 'block';
                expensesForm.style.display = 'block';
            }

        });
    </script>
@endsection
