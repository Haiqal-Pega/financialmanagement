@extends('layouts.nav')

@section('title', 'Dashboard')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="alert alert-info" role="alert">
        Please use the buttons below to navigate to different sections of the dashboard.
    </div>

    <div class="container-fluid mt-5 mt-5">
        <div class="row">
            <div class="col-md-6 mb-3">
                <a href="{{ route('budget-index') }}" class="btn btn-lg btn-block btn-primary d-flex align-items-center"
                    style="height: 150px">
                    <span class="mx-auto" style="font-size: 50px">Budget</span>
                </a>
            </div>
            <div class="col-md-6 mb-3">
                <a href="{{ route('bills-index') }}" class="btn btn-lg btn-block btn-success d-flex align-items-center"
                    style="height: 150px">
                    <span class="mx-auto" style="font-size: 50px">Bills</span>
                </a>
            </div>
            <div class="col-md-6 mb-3">
                <a href="{{ route('forecasts-index') }}" class="btn btn-lg btn-block btn-danger d-flex align-items-center"
                    style="height: 150px">
                    <span class="mx-auto" style="font-size: 50px">Forecasts</span>
                </a>
            </div>
            <div class="col-md-6 mb-3">
                <a href="{{ route('planning-index') }}" class="btn btn-lg btn-block btn-warning d-flex align-items-center"
                    style="height: 150px">
                    <span class="mx-auto" style="font-size: 50px">Plannings</span>
                </a>
            </div>
            <div class="col-md-12 mb-3">
                <a href="{{ route('report-index') }}" class="btn btn-lg btn-block btn-info d-flex align-items-center"
                    style="height: 150px">
                    <span class="mx-auto" style="font-size: 50px">Report</span>
                </a>
            </div>
        </div>
    </div>


    {{-- <div class="container mt-4">
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="card h-100 d-flex flex-column">
                    <div class="card-body">
                        <h5 class="card-title">Savings Overview</h5>
                        @if ($user->savingPlan)
                            <div class="card-body">
                                <div id="saving-chart"></div>
                                <canvas id="mySaving" width="100" height="50"
                                    data-current-amount="{{ $savings->current_amount }}"
                                    data-target-amount="{{ $savings->target_amount }}"></canvas>
                                <div class="row">
                                    <div class="col-sm-7">
                                        <label for="targetInitial" class="mt-4"><b>Saving Target:
                                                RM{{ $savings->target_amount }}</b></label>
                                    </div>
                                    <div class="col-sm-5 float-right">
                                        <a href="{{ route('savings.add') }}" class="btn btn-primary mt-3 float-right">Add To
                                            Saving</a>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="card-body">
                                <p>You haven't set up a saving plan yet.</p>
                                <a href="{{ route('savings.create') }}" class="btn btn-info">Edit</a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card h-100 d-flex flex-column">
                    <div class="card-body">
                        <h5 class="card-title">Stock Performance</h5>
                        <canvas id="myChart1" width="400" height="200" class="pt-5"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Recent Transactions</h5>
                        @if ($expenses->count() > 0)
                            <div class="list-group">
                                <table class="table table-sm">
                                    <thead>
                                        <tr>
                                            <th>Expense Name</th>
                                            <th>Amount (RM)</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $rowLimit = 5; @endphp
                                        @php $rowCount = 0; @endphp
                                        @foreach ($expenses as $expense)
                                            @php $rowCount++; @endphp
                                            @if ($rowCount <= $rowLimit)
                                                <tr>
                                                    <td>{{ $expense->name }}</td>
                                                    <td>{{ number_format($expense->amount, 2) }}</td>
                                                    <td>{{ $expense->created_at->format('d-M') }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tbody>
                                </table>
                            </div>
                            @if ($rowCount > $rowLimit)
                                <div class="row">
                                    <div class="col-sm-8">
                                        <p class="mt-1">Showing {{ $rowLimit }} recent transactions only.</p>
                                    </div>
                                    <div class="col-sm-4 float-right">
                                        <a href="{{ route('report-index') }}" class="btn btn-primary float-right">Show
                                            All</a>
                                    </div>
                                </div>
                            @endif
                        @else
                            <p>No transactions found.</p>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Upcoming Bills</h5>
                        <ul class="list-group">
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Electric Bill
                                <span class="badge badge-primary badge-pill">$50</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Water Bill
                                <span class="badge badge-primary badge-pill">$40</span>
                            </li>
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                Internet
                                <span class="badge badge-primary badge-pill">$80</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var xValues = ["Current", "Balance"];
        var canvas = document.getElementById("mySaving");
        var current = parseFloat(canvas.dataset.currentAmount);
        var target = parseFloat(canvas.dataset.targetAmount);
        var bal = target - current;
        var yValues = [current, bal];

        var barColors = [
            "#957DAD",
            "#E0BBE4",
        ];

        new Chart("mySaving", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            // options: {
            //     title: {
            //         display: true,
            //         text: "Investment Distribution"
            //     }
            // }
        });
        const xValues1 = [50, 60, 70, 80, 90, 100, 110, 120, 130, 140, 150];
        const yValues1 = [7, 8, 8, 9, 9, 9, 10, 11, 14, 14, 15];

        new Chart("myChart1", {
            type: "line",
            data: {
                labels: xValues1,
                datasets: [{
                    fill: false,
                    lineTension: 0,
                    backgroundColor: "rgba(0,0,255,1.0)",
                    borderColor: "rgba(0,0,255,0.1)",
                    data: yValues1
                }]
            },
            options: {
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        ticks: {
                            min: 6,
                            max: 16
                        }
                    }],
                }
            }
        });
    </script> --}}
@endsection
