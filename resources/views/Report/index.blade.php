@extends('layouts.nav')

@section('title', 'Financial Reports')

@section('content')
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="container mt-4">
        <h1 class="">Financial Reports</h1>

        <div class="row">
            <div class="col-md-4 d-flex">
                <div class="card flex-fill">
                    <div class="card-header"  style="background-color: #F0DDEC">
                        <h3>User Details</h3>
                    </div>
                    <div class="card-body">
                        <h5>Name: {{ $user->name }}</h5>
                        <h5>Email: {{ $user->email }}</h5>
                        <!-- Add more user information fields as needed -->
                    </div>
                </div>
            </div>
            <div class="col-md-8 d-flex">
                <div class="card flex-fill">
                    <div class="card-header"  style="background-color: #F0DDEC">
                        <h3>Summary</h3>
                    </div>
                    <div class="card-body">
                        <h5>Total Savings: RM {{ $savings }}</h5>
                        <h5>Accounts:</h5>
                        <ul>
                            @foreach ($user->accounts as $account)
                                <li>{{ $account->name }} (Initial: RM {{ number_format($account->balance, 2) }}, Balance:
                                    RM {{ number_format($account->getCurrentBalance(), 2) }})</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="card mt-4">
            <div class="card-header"  style="background-color: #F0DDEC">
                <h3>Detailed Transaction Reports</h3>
            </div>
            <div class="card-body">
                @if ($expenses->count() > 0)
                    <div class="list-group">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 17%">Expense Name</th>
                                    <th style="width: 22%">Description</th>
                                    <th style="width: 15%">Amount (RM)</th>
                                    <th style="width: 15%">Category</th>
                                    <th style="width: 15%">Account</th>
                                    <th style="width: 14%">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $count = 0; @endphp
                                @foreach ($expenses as $expense)
                                    @php $count++; @endphp
                                    @if ($count <= 3)
                                        <tr>
                                            <td>{{ $expense->name }}</td>
                                            <td>{{ $expense->description ?? '-Not Available-' }}</td>
                                            <td>{{ number_format($expense->amount, 2) }}</td>
                                            <td>{{ $expense->budget->name ?? '-not available' }}</td>
                                            <td>{{ $expense->account->name }}</td>
                                            <td>{{ $expense->created_at->format('d-M-y') }}</td>
                                        </tr>
                                    @else
                                    @break
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                @if ($expenses->count() > 3)
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#transactionModal"
                        style="width: 100%">
                        View All Transactions
                    </button>
                @endif
            @else
                <p>No transactions found.</p>
            @endif
        </div>
    </div>

    <div class="card mb-3 mt-4">
        <div class="card-header"  style="background-color: #F0DDEC">
            <div class="float-left"><b>Budget Balances</b></div>
            <form action="{{ route('report-filterByMonth') }}" method="GET">
                <div class="mb-3 float-right">
                    <button type="submit" class="btn btn-primary float-right ml-2" style="width: 120px">Filter</button>
                    <select class="form-control float-right" id="monthSelect" name="selected_month" style="width: 180px">
                        {{-- @if ($filtered)
                            <!-- Remove the "selected" attribute here -->
                            <option value="{{ $filtered }}" disabled>{{ $filtered }}</option>
                        @endif --}}
                        
                        @foreach ($months as $m)
                            <option value="{{ $m->month }}" {{ $m->month == $filtered ? 'selected' : '' }}>
                                {{ $m->month }}
                            </option>
                        @endforeach
                    </select>                    
                </div>
            </form>
            <div class="clearfix"></div>
        </div>
        <div class="card-body">
            @if ($budgetMonth->count() > 0)
                @foreach ($budgetMonth as $budget)
                    @if ($budget->amount > 0)
                        <div class="row mb-4">
                            <div class="col-sm-1">
                                <h5 class="card-title">{{ $budget->name }}</h5>
                            </div>
                            <div class="col-sm-7">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-lg" role="progressbar"
                                        style="width: {{ $budget->balancePercentage() }}%;"
                                        aria-valuenow="{{ $budget->balancePercentage() }}" aria-valuemin="0"
                                        aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="progress-details">
                                    {{ $budget->balancePercentage() }}% (RM
                                    {{ number_format($budget->amount - $budget->balance(), 2) }} /
                                    RM {{ number_format($budget->amount, 2) }})
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <p style="color: red">*Enter Salary for {{ date('F') }} to see budgets distribution*</p>
            @endif
        </div>
    </div>

    <!-- Transaction Modal -->
    <div class="modal fade" id="transactionModal" tabindex="-1" role="dialog" aria-labelledby="transactionModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="transactionModalLabel">All Transactions</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive" style="max-height: 800px; overflow-y: auto;">
                        <table class="table table-sm">
                            <thead>
                                <tr>
                                    <th style="width: 17%">Expense Name</th>
                                    <th style="width: 22%">Description</th>
                                    <th style="width: 15%">Amount (RM)</th>
                                    <th style="width: 15%">Category</th>
                                    <th style="width: 15%">Account</th>
                                    <th style="width: 14%">Date</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($expenses as $expense)
                                    <tr>
                                        <td>{{ $expense->name }}</td>
                                        <td>{{ $expense->description ?? '-Not Available-' }}</td>
                                        <td>{{ number_format($expense->amount, 2) }}</td>
                                        <td>{{ $expense->budget->name ?? 'not available ' }}</td>
                                        <td>{{ $expense->account->name }}</td>
                                        <td>{{ $expense->created_at->format('d-M-y') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
