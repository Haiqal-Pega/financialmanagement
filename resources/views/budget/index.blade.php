@extends('layouts.nav')

@section('title', 'Financial Manager')

@section('content')
    <div class="container">
        <h1>Budgets</h1>
        <div class="row">
            <div class="col-md-7">
                <div class="card mb-3" style="min-height: 350px">
                    <div class="card-header" style="background-color: #F8CEDC"><b>Account</b></div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($user->accounts as $account)
                                <div class="col-md-6">
                                    <div class="card">
                                        <div class="card-body">
                                            <h5 class="account-card">
                                                <label
                                                    for="{{ $account->name }}-label"><b>{{ $account->name }}</b></label><br>
                                                <label for="{{ $account->name }}-label">Initial: RM
                                                    {{ number_format($account->balance, 2) }}</label><br>
                                                <label for="{{ $account->name }}-label">Current: RM
                                                    {{ number_format($account->getCurrentBalance(), 2) }}</label>
                                            </h5>
                                            <a href="{{ route('accounts.edit', ['account' => $account->id]) }}"
                                                class="btn btn-info">Edit</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-5 pb-3">
                <div class="card" style="min-height: 350px">
                    <div class="card-header" style="background-color: #F8CEDC">
                        <b>Top Savings Plans</b>
                        @if ($plannings && $savings->count() > 0)
                            <a href="{{ route('savings.create') }}"
                                    class="btn btn-primary float-right btn-sm">Add Saving Plan</a>
                        @endif
                    </div>
                    <div class="card-body">
                        @php
                            $plan = 0;
                            $includeRetirement = true;
                        @endphp
                        @if ($plannings && $includeRetirement)
                            <div class="mb-2">
                                <h5 class="card-title">Retirement Saving</h5>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-lg" role="progressbar"
                                        style="width: {{ ($plannings->currentBalance / $plannings->projected_savings) * 100 }}%;"
                                        aria-valuenow="{{ ($plannings->currentBalance / $plannings->projected_savings) * 100 }}"
                                        aria-valuemin="0" aria-valuemax="100">
                                    </div>
                                </div>
                                <div class="progress-details">
                                    {{ number_format(($plannings->currentBalance / $plannings->projected_savings) * 100, 2) }}%
                                    (RM
                                    {{ number_format($plannings->currentBalance, 2) }} / RM
                                    {{ number_format($plannings->projected_savings, 2) }})
                                </div>
                            </div>
                        @endif
                        @if ($savings->count() > 0)
                            @foreach ($savings as $saving)
                                @php $plan++; @endphp
                                @if ($plan <= ($plannings ? 2 : 3))
                                    <div class="mb-2">
                                        <h5 class="card-title">{{ $saving->name }}</h5>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-lg" role="progressbar"
                                                style="width: {{ ($saving->current_amount / $saving->target_amount) * 100 }}%;"
                                                aria-valuenow="{{ ($saving->current_amount / $saving->target_amount) * 100 }}"
                                                aria-valuemin="0" aria-valuemax="100">
                                            </div>
                                        </div>
                                        <div class="progress-details">
                                            {{ number_format(($saving->current_amount / $saving->target_amount) * 100, 2) }}%
                                            (RM
                                            {{ number_format($saving->current_amount, 2) }} / RM
                                            {{ number_format($saving->target_amount, 2) }})
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        @endif


                        @if (!$plannings && $savings->count() == 0)
                            <b>No Saving Plans Set <a href="{{ route('savings.create') }}"
                                    class="btn btn-primary float-right">Add Saving Plan</a></b>
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header" style="background-color: #F8CEDC">
            <div class="float-left"><b>Budget Balances ({{ date('F') }})</b></div>
            <a href="{{ route('budgets.edit') }}" class="btn btn-primary float-right">Enter Salary</a>
            <div class="clearfix"></div>
        </div>
        <div class="card-body">
            @if ($budgets->count() > 0)
                @foreach ($budgets as $budget)
                    @if ($budget->amount > 0)
                        <div class="row mb-4">
                            <div class="col-sm-1">
                                <h5 class="card-title">{{ $budget->name }}</h5>
                            </div>
                            <div class="col-sm-7">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-lg" role="progressbar"
                                        style="width: {{ $budget->balancePercentage() }}%;"
                                        aria-valuenow="{{ $budget->balancePercentage() }}" aria-valuemin="0"
                                        aria-valuemax="100">
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="progress-details">
                                    {{ $budget->balancePercentage() }}% (RM
                                    {{ number_format($budget->amount - $budget->balance(), 2) }} /
                                    RM {{ number_format($budget->amount, 2) }})
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            @else
                <p style="color: red">*Enter Salary for {{ date('F') }} to see budgets distribution*</p>
            @endif
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header" style="background-color: #F8CEDC">
            <div class="float-left"><b>5 Latest Transactions</b></div>
            <a href="{{ route('expenses.index') }}" class="btn btn-primary float-right">Add New Expenses</a>
            <div class="clearfix"></div>
        </div>
        <div class="card-body">
            @if ($expenses->count() > 0)
                <div class="list-group">
                    <table class="table table-sm table-hover">
                        <thead >
                            <tr>
                                <th style="width: 17%">Expense Name</th>
                                <th style="width: 22%">Description</th>
                                <th style="width: 15%">Amount (RM)</th>
                                <th style="width: 15%">Category</th>
                                <th style="width: 15%">Account</th>
                                <th style="width: 14%">Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php $count = 0; @endphp
                            @foreach ($expenses as $expense)
                                @php $count++; @endphp
                                @if ($count <= 5)
                                    <tr>
                                        <td>{{ $expense->name }}</td>
                                        <td>{{ $expense->description ?? '-Not Available-' }}</td>
                                        <td>{{ number_format($expense->amount, 2) }}</td>
                                        <td>{{ $expense->budget->name ?? '-not available' }}</td>
                                        <td>{{ $expense->account->name }}</td>
                                        <td>{{ $expense->created_at->format('d-M-y') }}</td>
                                    </tr>
                                @else
                                @break
                            @endif
                        @endforeach
                    </tbody>
                </table>
            </div>
        @else
            <p style="color: red">*No transactions found*</p>
        @endif

    </div>

</div>
</div>
<script>
    var canvas = document.getElementById("mySaving");

    canvas.innerHTML = ''; // Clear any existing content in the canvas

    savingsData.forEach(plan => {
        var progressContainer = document.createElement("div");
        progressContainer.classList.add("mb-3");

        var progressBar = document.createElement("div");
        progressBar.classList.add("progress");

        var progressInner = document.createElement("div");
        progressInner.classList.add("progress-bar");
        progressInner.classList.add("progress-bar-lg");
        progressInner.role = "progressbar";
        progressInner.style.width = ((plan.current_amount / plan.target_amount) * 100) + "%";
        progressInner.ariaValuenow = (plan.current_amount / plan.target_amount) * 100;
        progressInner.ariaValuemin = 0;
        progressInner.ariaValuemax = 100;

        progressBar.appendChild(progressInner);
        progressContainer.appendChild(progressBar);

        var progressDetails = document.createElement("div");
        progressDetails.classList.add("progress-details");
        progressDetails.textContent =
            `${((plan.current_amount / plan.target_amount) * 100).toFixed(2)}% (RM ${Number(plan.target_amount - plan.current_amount).toFixed(2)} / RM ${Number(plan.target_amount).toFixed(2)})`;

        progressContainer.appendChild(progressDetails);
        canvas.appendChild(progressContainer);
    });
</script>
@endsection
