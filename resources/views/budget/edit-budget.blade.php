@extends('layouts.nav')

@section('title', 'Setup Budget')

@section('content')
    <div class="container">
        <h1>Salary for the Month</h1>
        @if ($salarySet)
            <div class="alert alert-info" role="alert">
                Salary for {{ date('F') }} has already been set.
            </div>
        @else
            <div class="card">
                <div class="card-body">
                    <form method="POST" action="{{ route('budget-store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="salary">Enter Salary for {{ date('F') }}</label>
                            <input type="number" id="salary" name="salary" class="form-control" required>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="Needs">Needs (50%)</label>
                                <input type="text" id="needs" name="needs" class="form-control" disabled>
                                <!-- Add a hidden input for Needs -->
                                <input type="hidden" id="needsHidden" name="needsHidden">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="Wants">Wants (30%)</label>
                                <input type="text" id="wants" name="wants" class="form-control" disabled>
                                <!-- Add a hidden input for Wants -->
                                <input type="hidden" id="wantsHidden" name="wantsHidden">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="savings">Savings (20%)</label>
                                <input type="text" id="savings" name="savings" class="form-control" disabled>
                                <!-- Add a hidden input for Savings -->
                                <input type="hidden" id="savingsHidden" name="savingsHidden">
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Confirm</button>
                            <a href="{{ route('budget-index') }}" class="btn btn-secondary">Cancel</a>
                        </div>

                    </form>
                </div>
            </div>
        @endif
    </div>

    <script>
        const salaryInput = document.getElementById('salary');
        const needsInput = document.getElementById('needs');
        const wantsInput = document.getElementById('wants');
        const savingsInput = document.getElementById('savings');

        salaryInput.addEventListener('input', calculatePercentages);

        function calculatePercentages() {
            const salary = parseFloat(salaryInput.value);
            if (isNaN(salary)) {
                needsInput.value = '';
                wantsInput.value = '';
                savingsInput.value = '';
                return;
            }

            const needsPercentage = 50;
            const wantsPercentage = 30;
            const savingsPercentage = 20;

            const needsAmount = (needsPercentage / 100) * salary;
            const wantsAmount = (wantsPercentage / 100) * salary;
            const savingsAmount = (savingsPercentage / 100) * salary;

            needsInput.value = needsAmount.toFixed(2);
            wantsInput.value = wantsAmount.toFixed(2);
            savingsInput.value = savingsAmount.toFixed(2);

            // Update the hidden input values
            document.getElementById('needsHidden').value = needsAmount.toFixed(2);
            document.getElementById('wantsHidden').value = wantsAmount.toFixed(2);
            document.getElementById('savingsHidden').value = savingsAmount.toFixed(2);
        }
    </script>
@endsection
