@extends('layouts.nav')

@section('title', 'Set Up Saving')

@section('content')
    <div class="container">
        <h1>Set Up Saving</h1>
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('savings.store') }}">
                    @csrf

                    <div class="form-group">
                        <label for="name">Saving Name</label>
                        <input type="text" id="name" name="name" class="form-control" required value="Saving Plan">
                    </div>                    

                    <div class="form-group">
                        <label for="target_amount">Target Amount</label>
                        <input type="number" id="target_amount" name="target_amount" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="deadline">Deadline</label>
                        <input type="date" id="deadline" name="deadline" class="form-control" required>
                    </div>

                    {{-- <div class="form-group">
                        <label for="account">Select Account</label>
                        <select id="account" name="account" class="form-control" required>
                            @foreach ($user->accounts as $account)
                                <option value="{{ $account->id }}">{{ $account->name }}</option>
                            @endforeach
                        </select>
                    </div> --}}

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Set Up Saving</button>
                        <a href="{{ route('budget-index') }}" class="btn btn-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
