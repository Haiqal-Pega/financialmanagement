@extends('layouts.nav')

@section('title', 'Add Amount to Saving')

@section('content')
    <div class="container">
        <h1>Add Amount to Saving</h1>
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{ route('savings.update') }}">
                    @csrf
                    
                    <div class="form-group">
                        <label for="amount">Amount</label>
                        <input type="number" id="amount" name="amount" class="form-control" required step="0.01">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                        <label for="disclaimer" style="color: red">*This action will deduct from initial amount from the bank account*</label>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Add Amount</button>
                        <a href="{{ route('budget-index') }}" class="btn btn-secondary">Cancel</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
