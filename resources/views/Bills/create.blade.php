@extends('layouts.nav')
@section('title', 'Set Up Bills')
@section('content')
    <div class="container">
        <div class="shadow p-5 my-5 bg-white rounded">
            <h1>Add Bills</h1>
            <form method="POST" action="{{ route('bills.store') }}">
                @csrf
                <div class="form-group">
                    <label for="name">Bill Name</label>
                    <input type="text" class="form-control" id="name" name="name" required>
                </div>
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <input type="number" class="form-control" id="amount" name="amount" step="0.01" required>
                </div>
                <div class="form-group">
                    <label for="due_date">Due Date</label>
                    <input type="date" class="form-control" id="due_date" name="due_date" required>
                </div>
                <button type="submit" class="btn btn-primary">Confirm Bill</button>
            </form>
        </div>
    </div>
@endsection
