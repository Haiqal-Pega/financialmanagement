@extends('layouts.nav')

@section('title', 'Financial Manager')

@section('content')
    <div class="container">
        <h1>Bills Payment</h1>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <a href="{{ route('bills.create') }}" class="btn btn-primary float-right mb-3" style="width: 120px">Add
                            Bills</a>
                        <h5 class="card-title">Bill Reminder</h5>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 50%">Bill</th>
                                    <th style="width: 20%">Due Date</th>
                                    <th style="width: 15%; text-align:start" class="">Amount</th>
                                    <th style="width: 10%; text-align:end"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($bills->count() > 0)
                                    @foreach ($bills as $bill)
                                        <tr>
                                            <td>{{ $bill->name }}</td>
                                            <td>{{ $bill->due_date }}</td>
                                            <td class="text-start " style="text-align:start">RM {{ $bill->amount }}</td>
                                            <td class="text-center" style="">
                                                <button class="btn btn-sm btn-success" style="min-width: 60px"
                                                    type="button"
                                                    onclick="confirmPayment('{{ $bill->name }}', {{ $bill->amount }}, {{ $bill->id }})">Pay</button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">
                                            <p style="color: red">*No bills for this month*</p>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-12 mt-4">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('bills.filterByMonth') }}" method="GET">
                            <div class="mb-3 float-right">
                                <button type="submit" class="btn btn-primary float-right ml-2"
                                    style="width: 120px">Filter</button>
                                <select class="form-control float-right" id="monthSelect" name="selected_month"
                                    style="width: 180px">
                                    <option value='null' selected>Choose Month</option>
                                    @foreach ($months as $m)
                                        <option value="{{ $m->month }}">{{ $m->month }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </form>
                        <h5 class="card-title">Payment History</h5>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 25%">Payment Date</th>
                                    <th style="width: 35%">Bill Name</th>
                                    <th style="width: 25%">Amount</th>
                                    <th style="width: 15%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($pastBills->count() > 0)
                                    @foreach ($pastBills as $past)
                                        <tr>
                                            <td>
                                                @if ($past->status == 'Y')
                                                    {{ substr($past->updated_at, 0, 10) }}
                                                @else
                                                    <p style="color: red"><b> {{ substr($past->due_date, 0, 10) }}</b></p>

                                                   
                                                @endif
                                            </td>
                                            <td>{{ $past->name }}</td>
                                            <td class="" style="text-align:start">RM {{ $past->amount }}
                                            </td>
                                            <td>
                                                @if ($past->status == 'Y')
                                                    PAID
                                                @else
                                                    <p style="color: red"><b>UNPAID</b></p>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="3">
                                            <p style="color: red">*No history*</p>
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function confirmPayment(billName, amount, id) {
            const confirmation = window.confirm(`Are you sure you want to pay RM ${amount} for ${billName}?`);

            if (confirmation) {
                window.location.href = `/Bills/payment/${id}`;
            }
        }
    </script>

@endsection
