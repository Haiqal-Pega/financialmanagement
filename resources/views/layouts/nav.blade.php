<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/bootstrap-icons.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>


    <style>
        @media (min-width: 1200px) {
            .modal-xl {
                width: 90%;
                max-width: 1200px;
            }
        }

        /* @media (min-height: 1200px) {
            .modal-xl {
                height: 90%;
                min-height: 1200px;
            }
        } */

        body {
            display: flex;
            flex-direction: column;
            min-height: 100vh;
        }

        .content {
            flex: 1;
            padding-bottom: 60px;
            /* Adjust this value to leave space for the footer */
        }

        footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            height: 60px;
            background-color: #f8f9fa;
            padding: 20px;
            text-align: center;
            display: none;
            /* Hide the footer by default */
            transition: all 0.3s ease-in-out;
            /* Add smooth transition effect */
        }

        footer.visible {
            display: block;
            /* Show the footer when it has the 'visible' class */
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="#">Financial Manager</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    {{-- <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('home') }}">Home</a>
                    </li> --}}
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('budget-index') }}">Budget</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('bills-index') }}">Bills</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('forecasts-index')}}">Forecasts</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('planning-index') }}">Planning</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link px-3" href="{{ route('report-index') }}">Reports</a>
                    </li>
                    <li class="nav-item">
                        <form action="{{ route('logout') }}" method="POST">
                            <!-- Change the HTTP method to POST -->
                            @csrf
                            <!-- Add the CSRF token -->
                            <button type="submit" class="btn btn-light">Logout</button>
                            <!-- Fix the background color -->
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>


    <div class="content" style="background-color: #FCEEE5">
        <div class="container mt-4">
            @yield('content')
        </div>
    </div>

    <footer>
        <div class="container">
            &copy; 2023 Financial Manager. All rights reserved.
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        // Function to toggle footer visibility
        function toggleFooter() {
            const footer = document.querySelector('footer');
            if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {
                footer.classList.add('visible');
            } else {
                footer.classList.remove('visible');
            }
        }

        // Event listener for scroll event
        window.addEventListener('scroll', toggleFooter);
    </script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>

</body>

</html>
