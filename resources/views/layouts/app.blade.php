<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/bootstrap-icons.min.css" rel="stylesheet">

    <style>
        html, body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
        .wrapper {
            min-height: 100%;
            display: flex;
            flex-direction: column;
            background-color: #2c3e50;
        }
        .content {
            flex: 1;
        }
        .bg-light {
            background-color: #2c3e50 !important; /* Set header and footer background color to a light gray */
            color: #FFFFFF;
        }
        body {
            color: #333333; /* Set text color to a dark gray */
        }
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #2c3e50; /* Set footer background color to a light gray */
            text-align: center;
            padding: 10px 0;
            color: white; /* Set footer text color to a dark gray */
        }
    </style>
</head>
<body>
    <div class="wrapper">
        <header class="bg-light py-3">
            <div class="container text-center">
                <h1 class="mb-0">Welcome to Financial Management</h1>
            </div>
        </header>

        <div class="content" style="background-image: url('/images/defaultImg.jpg')">
            @yield('content')
        </div>

        <footer class="footer">
            <div class="container">
                <p>&copy; 2023 Financial Manager. All rights reserved.</p>
            </div>
        </footer>
    </div>

    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>
