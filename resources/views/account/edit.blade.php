@extends('layouts.nav')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-header">Edit Account</div>
            <div class="card-body">
                <form action="{{ route('accounts.update', $account->id) }}" method="POST">
                    @csrf
                    @method('PUT')

                    <div class="form-group">
                        <label for="name">Account Name</label>
                        <input disabled type="text" id="name" name="name" class="form-control" value="{{ $account->name }}">
                    </div>

                    <div class="form-group">
                        <label for="balance">Account Balance</label>
                        <input type="number" id="balance" name="balance" class="form-control" value="{{ $account->balance }} step="0.01" min="0">
                    </div>

                    <button type="submit" class="btn btn-primary">Update Account</button>
                </form>
            </div>
        </div>
    </div>
@endsection
