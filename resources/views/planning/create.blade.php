@extends('layouts.nav')

@section('title', 'Financial Manager')
@section('content')
    <div class="container d-flex justify-content-center">
        <div class="card mt-3" style="width: 30%">
            <div class="card-header"><b>Create Retirement Plan</b></div>
            <div class="card-body">
                <form action="{{ route('planning-store') }}" method="POST">
                    @csrf

                    <div class="form-group">
                        <label for="currentAge">Current Age:</label>
                        <input type="number" id="currentAge" name="currentAge" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="targetAge">Targeted Age to Retire:</label>
                        <input type="number" id="targetAge" name="targetAge" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="projected_savings">Planned Retirement Savings (RM):</label>
                        <input type="number" id="projected_savings" name="projected_savings" class="form-control" required>
                    </div>

                    <div class="form-group">
                        <label for="monthly_savings">You should save (RM):</label>
                        <input type="number" id="monthly_savings" name="monthly_savings" class="form-control" disabled>
                    </div>

                    <!-- Add more fields as needed -->

                    <button type="submit" class="btn btn-primary float-right mt-3">Create Plan</button>
                </form>
            </div>
        </div>
    </div>

    <script>
        const currentAgeInput = document.getElementById('currentAge');
        const targetAgeInput = document.getElementById('targetAge');
        const projectedSavingsInput = document.getElementById('projected_savings');
        const monthlySavingsInput = document.getElementById('monthly_savings');

        currentAgeInput.addEventListener('input', calculateMonthlySavings);
        targetAgeInput.addEventListener('input', calculateMonthlySavings);
        projectedSavingsInput.addEventListener('input', calculateMonthlySavings);

        function calculateMonthlySavings() {
            const currentAge = parseInt(currentAgeInput.value);
            const targetAge = parseInt(targetAgeInput.value);
            const projectedSavings = parseFloat(projectedSavingsInput.value);

            if (!isNaN(currentAge) && !isNaN(targetAge) && !isNaN(projectedSavings)) {
                const yearsToSave = targetAge - currentAge;
                const monthlySavings = projectedSavings / (yearsToSave * 12);

                monthlySavingsInput.value = monthlySavings.toFixed(2);
            }
        }
    </script>
@endsection
