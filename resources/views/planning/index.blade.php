@extends('layouts.nav')

@section('title', 'Financial Manager')

@section('content')
    <div class="container">
        <h1>Financial Planning</h1>

        <div class="card mt-4">
            <div class="card-header"  style="background-color: #F0DDEC">
                <b>Retirement Plan</b>
            </div>
            <div class="card-body">
                @if ($retirementPlan)
                    <p>Age: {{ $retirementPlan->currentAge }}</p>
                    <p>Targeted Age to Retire: {{ $retirementPlan->targetAge }}</p>
                    <p>Suggest Savings per Month: RM {{ number_format($retirementPlan->monthly_savings, 2) }}</p>
                    <p>Progress: RM {{ number_format($retirementPlan->currentBalance,2) }}/RM {{ number_format($retirementPlan->projected_savings,2) }}</p>
                    <div class="progress mt-2">
                        <div class="progress-bar" role="progressbar"
                            style="width: {{ ($retirementPlan->currentBalance / $retirementPlan->projected_savings) * 100 }}%"
                            aria-valuenow="{{ ($retirementPlan->currentBalance / $retirementPlan->projected_savings) * 100 }}"
                            aria-valuemin="0" aria-valuemax="100">
                            {{ round(($retirementPlan->currentBalance / $retirementPlan->projected_savings) * 100, 2) }}%
                        </div>
                    </div>
                @else
                    No retirement plan data found.
                    <a href="{{ route('planning-create') }}" class="btn btn-primary float-right">Set Up Retirement Plan</a>
                @endif
            </div>
        </div>

        <div class="card mt-4">
            <div class="card-header" style="background-color: #F0DDEC">
                <b>Saving Plans Progress <a href="{{ route('savings.create') }}" class="btn btn-primary float-right">Add Saving Plan</a></b>
            </div>
            <div class="card-body">
                <div class="row d-flex justify-content-center">
                    @if (count($savings) > 0)
                        @foreach ($savings as $saving)
                            <div class="col-md-3 my-3 card m-2">
                                <div class="saving-plan p-2">
                                    <h3>{{ $saving->name }}</h3>
                                    <p>Target Amount: RM {{ number_format($saving->target_amount, 2) }}</p>
                                    <p>Ongoing: RM {{ number_format($saving->current_amount, 2) }}</p>
                                    <p>Due Date: {{ $saving->deadline }}</p>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <b style="color: red; text-align: center">***Use the top right button to create a saving plan***</b>
                    @endif
                </div>
            </div>            
        </div>
    </div>
@endsection
