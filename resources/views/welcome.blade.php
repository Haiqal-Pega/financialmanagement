<?php
session()->flush(); // Clear all session data
session()->regenerate(); // Generate a new session ID
?>
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome - Financial Management</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>

        body {
            min-height: 100vh;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            background-color: #f9f9f9;
        }

        .header {
            background-color: #2c3e50;
            padding: 20px;
            text-align: center;
            color: #ffffff;
            font-size: 24px;
        }

        .content {
            flex: 1;
            display: flex;
            align-items: center;
            justify-content: center;
        }

        .btn {
            font-size: 18px;
            text-decoration: none;
            border: none;
            cursor: pointer;
            transition: background-color 0.3s, color 0.3s;
        }

        .btn-primary {
            background-color: #3498db;
            color: #ffffff;
        }

        .btn-secondary {
            background-color: #e74c3c;
            color: #ffffff;
        }

        .footer {
            background-color: #2c3e50;
            padding: 10px;
            text-align: center;
            color: #ffffff;
            font-size: 14px;
        }

        .footer p {
            margin: 0;
        }
    </style>
</head>

<body>
    <header class="header">
        <div class="container">
            <div class="logo">
                <h1>Welcome to Financial Management</h1>
            </div>
        </div>
    </header>

    <div class="content" style="background-image: url('/images/defaultImg.jpg')">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8     ">
                    <div class="text-center mb-3">
                        <a href="{{ route('login') }}" class="btn btn-light p-3 m-2 shadow"
                            style="width: 400px; height: 60px"><i class="fas fa-sign-in-alt"></i>
                            <b>PRESS HERE TO LOGIN</a>
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="btn btn-info p-3 m-2"
                                style="width: 150px; height: 60px"><i class="fas fa-user-plus"></i>
                                Register Here</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p>&copy; 2023 Financial Management. All rights reserved.</p>
        </div>
    </footer>

    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
